from django import forms
from .models import Pengalaman

class FormPengalaman(forms.ModelForm):
    class Meta:
        model = Pengalaman
        fields = ('nama', 'cerita')

        widgets = {
            'nama': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder': "Nama kamu siapa?"}),

            'cerita': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder': "Ceritakan pengalaman kamu!"}),     
        }
