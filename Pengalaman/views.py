from django.shortcuts import render, redirect
from .forms import FormPengalaman
from .models import Pengalaman

# Create your views here.
def pengalamanPage(request):
    if request.method == 'POST':
        form = FormPengalaman(request.POST)
        if form.is_valid():
            form.save()
            form = FormPengalaman()
        listPengalaman = Pengalaman.objects.all()
        return render(request, 'Pengalaman.html', {'form':form, 'listPengalaman':listPengalaman})
    else:
        form = FormPengalaman()
        listPengalaman = Pengalaman.objects.all()
        return render(request, 'Pengalaman.html', {'form': form, 'listPengalaman':listPengalaman})