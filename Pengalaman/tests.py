from django.test import TestCase, Client
from .models import Pengalaman
from .forms import FormPengalaman
from .views import pengalamanPage
from django.apps import apps
from .apps import PengalamanConfig

# Create your tests here.
class TestPengalaman(TestCase):
    def test_GET_pengalaman(self):
        response = Client().get('/pengalaman')
        self.assertEquals(response.status_code, 200)

    def test_POST_pengalaman(self):
        response = self.client.post('/pengalaman', 
        {'nama':"aurel", 'cerita':"ini cerita pengalamanku"}, follow=True)
        self.assertEqual(response.status_code, 200)
        
    def test_template(self):
        response = Client().get('/pengalaman')
        self.assertTemplateUsed(response,'Pengalaman.html')
    