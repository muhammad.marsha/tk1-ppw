from django.shortcuts import render

from django.urls import path

from . import views

app_name = 'RumahSakit'

urlpatterns = [
    path('list_rumah_sakit', views.list_rs, name='list_rs'),
    path('pendaftar', views.list_pendaftar, name='pendaftar'),
]
