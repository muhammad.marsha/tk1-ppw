from django.shortcuts import render, redirect
from .forms import Form_Pendaftar
from .models import Pendaftar
from django.conf import settings
from django.conf.urls.static import static

# Create your views here.


def list_rs(request):
    if request.method == 'POST':
        form = Form_Pendaftar(request.POST,request.FILES or None)
        if form.is_valid():
            form.save()
            return redirect('RumahSakit:list_rs')
    else:
        form = Form_Pendaftar()
    return render(request, 'RumahSakit.html', {'form': form})

def list_pendaftar(request):
    hasilForm = Pendaftar.objects.all().order_by('tanggal')
    return render(request, 'listdaftar.html', {'hasilForm': hasilForm })


