from django import forms 
from .models import Pendaftar

class DateInput(forms.DateInput):
    input_type = 'date'
  
class Form_Pendaftar(forms.ModelForm):  
    class Meta: 
        model = Pendaftar
        fields = ('nama', 'telp', 'rumah_sakit', 'tanggal', 'jam', 'Bukti_Transfer')
        widgets = {
            'tanggal': DateInput()
        }

    def __init__(self, *args, **kwargs):
        super(Form_Pendaftar, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['nama'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'
        self.fields['telp'].widget.attrs['style']  = 'border-radius: 20px;margin:auto'
        self.fields['rumah_sakit'].widget.attrs['style'] = 'border-radius: 20px; margin:auto'
        self.fields['tanggal'].widget.attrs['style'] = 'border-radius: 20px; margin:auto'
        self.fields['jam'].widget.attrs['style'] = 'border-radius: 20px; margin:auto'
        # self.fields['Bukti_Transfer'].widget.attrs['style'] = 'border-radius:20px;'
        self.fields['nama'].widget.attrs.update({'placeholder': 'Masukkan Nama Lengkap', 'class':'text-center form-control'})
        self.fields['telp'].widget.attrs.update({'placeholder': 'Masukkan Nomor HP Anda', 'class':'text-center form-control'})
        self.fields['jam'].widget.attrs.update({'placeholder': 'Pilih Jam Pemeriksaan', 'class':'text-center form-control'})
        self.fields['rumah_sakit'].widget.attrs.update({'placeholder': 'Pilih Rumah Sakit', 'class':'text-center form-control'})
        self.fields['tanggal'].widget.attrs.update({'class':'text-center form-control'})