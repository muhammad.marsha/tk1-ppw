from django.shortcuts import render, redirect
from django.db.models import Sum
from django.conf import settings
from django.conf.urls.static import static

from .forms import FormDonatur
from .models import Donatur
# Create your views here.

def donatur(request):
    total = Donatur.objects.all()
    sum = 0
    for item in total :
        sum += item.getNominal()
    return render(request, 'donasi.html', {'jumlah': sum, 'total': total})

def donasi(request):
    # donatur_form = FormDonatur(request.POST,request.FILES or None)
    if request.method == 'POST':
        donatur_form = FormDonatur(request.POST,request.FILES or None)
        if donatur_form.is_valid():
            donatur_form.save()
            return redirect('../')
    else:
        donatur_form = FormDonatur()
    return render(request, "formDonasi.html", {'data_form': donatur_form})