from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Donatur
from django.apps import apps
from .views import donatur, donasi
from .forms import FormDonatur
from .apps import DonasiConfig
import tempfile
from django.http import HttpRequest
# Create your tests here.


class ModelTest(TestCase):
    def setUp(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        self.donatur = Donatur.objects.create(
            nama_donatur="marsha", nominal_donatur=20000,pesan_donatur="ini pesan", bukti_donatur=image)

    def test_instance_created(self):
        self.assertEqual(Donatur.objects.count(), 1)
    
    def test_str(self):
        self.assertEqual(str(self.donatur), "1. marsha")

    def test_nominal(self):
        self.assertEqual(self.donatur.getNominal(), 20000)

# class FormTest(TestCase):
#     def test_form_is_valid(self):

class UrlsTest(TestCase):
    def setUp(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        self.donatur = Donatur.objects.create(
            nama_donatur="marsha", nominal_donatur=20000,pesan_donatur="ini pesan", bukti_donatur=image)
        self.donatur = reverse("donasi:donatur")
        self.donasi = reverse("donasi:donasi")

    def test_donatur_use_right_function(self):
        found = resolve(self.donatur)
        self.assertEqual(found.func, donatur)
    
    def test_donasi_use_right_function(self):
        found = resolve(self.donasi)
        self.assertEqual(found.func, donasi)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.donatur = reverse("donasi:donatur")
        self.donasi = reverse("donasi:donasi")

    def test_GET_donatur(self):
        response = self.client.get(self.donatur)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'donasi.html')

    def test_GET_donasi(self):
        response = self.client.get(self.donasi)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formDonasi.html')

    def test_POST_donasi(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        response = self.client.post(self.donasi,
        { 'nama_donatur':"marsha", 'nominal_donatur':20000,'pesan_donatur':"ini pesan", 'bukti_donatur':image}, follow=True)
        self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(DonasiConfig.name, 'donasi')
        self.assertEqual(apps.get_app_config('donasi').name, 'donasi')    