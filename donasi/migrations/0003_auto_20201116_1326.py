# Generated by Django 3.1.2 on 2020-11-16 06:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('donasi', '0002_donatur_bukti_donatur'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donatur',
            name='bukti_donatur',
            field=models.FileField(null=True, upload_to='donatur/'),
        ),
    ]
