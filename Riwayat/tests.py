from django.test import TestCase, Client
from django.urls import resolve
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpRequest
from .views import list_riwayat, form_riwayat
from .models import Riwayat
import tempfile

# Create your tests here.

class tes_hasil_riwayat(TestCase):
    
    def test_url_is_exist(self):
        response = Client().get('/list_riwayat')
        self.assertEqual(response.status_code, 200)

    def test_list_riwayat_using_index_func(self):
        found = resolve('/list_riwayat')
        self.assertEqual(found.func, list_riwayat)

    def test_template_used(self):
        response =Client().get('/list_riwayat')
        self.assertTemplateUsed(response, 'list_riwayat.html')

    def test_html_contains(self):
        request = HttpRequest()
        response = list_riwayat(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Riwayat Perjalanan Pasien Covid', html_response)
        self.assertIn('Tempat Dikunjungi', html_response)
        self.assertIn('Tanggal', html_response)

    def test_daftar_swab(self):
        riwayat1 = Riwayat(nama='rahmat', alamat='bogor', tempat='indomaret', tanggal='2011-11-11')
        riwayat1.save()
        riwayat2 = Riwayat.objects.create(nama='inul', alamat='bogor', tempat='alfamart', tanggal='2012-12-12')
        self.assertEqual(Riwayat.objects.all().count(), 2)
    
    def test_daftar_swab_url_is_exist(self):
        response = Client().post('/list_riwayat', data = {'nama':'rahmat', 'alamat':'bogor', 'tempat':'indomaret', 'tanggal':'2011-11-11'})
        self.assertEqual(response.status_code, 200)

class form_riwayat(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/form_riwayat')
        self.assertEqual(response.status_code, 200)

    # def test_form_riwayat_using_index_func(self):
    #     found = resolve('/form_riwayat')
    #     self.assertEqual(found.func, form_riwayat)

    def test_template_used(self):
        response = Client().get('/form_riwayat')
        self.assertTemplateUsed(response, 'form_riwayat.html')

    # def test_html_contains(self):
    #     request = HttpRequest()
    #     response = form_riwayat(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Nama', html_response)
    #     self.assertIn('Alamat', html_response)
    #     self.assertIn('Tempat Dikunjungi', html_response)
    #     self.assertIn('Tanggal', html_response)
    #     self.assertIn('Submit', html_response)