from django import forms 
from .models import Riwayat

class DateInput(forms.DateInput):
    input_type = 'date'
  
class Form_Riwayat(forms.ModelForm):  
    class Meta: 
        model = Riwayat
        fields = ('nama', 'alamat', 'tempat', 'tanggal')
        widgets = {
            'tanggal': DateInput()
        }

    def __init__(self, *args, **kwargs):
        super(Form_Riwayat, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['nama'].widget.attrs['style'] = 'width:500px; height:40px; border-radius : 5px;'
        self.fields['alamat'].widget.attrs['style']  = 'width:500px; height:40px; border-radius : 5px;'
        self.fields['tempat'].widget.attrs['style'] = 'width:500px; height:100px; border-radius : 5px;'
        self.fields['tanggal'].widget.attrs['style'] = 'width:500px; height:40px; border-radius : 5px;'
        