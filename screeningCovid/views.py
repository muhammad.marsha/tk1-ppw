from django.shortcuts import render, redirect
from .models import Isian
from .forms import Form_bantuan

# Create your views here.
def bantuan(request):
    if request.method == "POST":
        form = Form_bantuan(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'haltidak.html')
    else :
        form = Form_bantuan()
    return render(request, 'halscreening.html', {'form': form})

def pendaftar_bantuan(request):
    hasilForm = Isian.objects.all()
    return render(request,'haldaftar.html',{'hasilForm': hasilForm})
