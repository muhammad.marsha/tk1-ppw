from django import forms
from .models import Isian

class Form_bantuan(forms.ModelForm):
    class Meta:
        model = Isian
        fields = ('nama', 'nohp', 'alamat')

        widgets = {
            'nama': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder': "Isi dengan nama lengkapmu!"}),

            'nohp': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': "Isi dengan nomer yang dapat dihubungi!"}),

            'alamat': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder': "Isi dengan alamat lengkap!"}),
        }