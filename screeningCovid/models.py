from django.db import models

# Create your models here.
class Isian(models.Model):
    nama = models.CharField('Nama', max_length = 50) 
    nohp = models.PositiveIntegerField('Nomor Hp')
    alamat = models.CharField('Alamat', max_length = 3000)

    def __str__(self):
        return self.nama